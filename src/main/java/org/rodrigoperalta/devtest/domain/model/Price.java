package org.rodrigoperalta.devtest.domain.model;

import lombok.Value;
import org.rodrigoperalta.devtest.application.config.exceptions.GenericDomainException;

import java.util.Objects;

@Value
public class Price {

    Double value;

    public static Price create(final Double price) {

        if (Objects.isNull(price)) {
            throw new NullPriceException();
        }

        if (price < 0) {
            throw new NegativePriceException();
        }

        return new Price(price);

    }

    public static class NullPriceException extends GenericDomainException {

        private NullPriceException() {
            super("Price cannot have a null value.");
        }

    }


    public static class NegativePriceException extends GenericDomainException {

        private NegativePriceException() {
            super("Price cannot have a negative value.");
        }

    }

}
