package org.rodrigoperalta.devtest.domain.model;

import lombok.Value;
import org.rodrigoperalta.devtest.application.config.exceptions.GenericDomainException;

import java.util.Objects;

@Value
public class Id {

    String value;

    public static Id create(final String id) {

        if (Objects.isNull(id)) {
            throw new NullIdException();
        }

        if (id.isBlank()) {
            throw new BlankIdException();
        }

        return new Id(id);

    }


    public static class NullIdException extends GenericDomainException {

        private NullIdException() {
            super("Id cannot have a null value.");
        }

    }


    public static class BlankIdException extends GenericDomainException {

        private BlankIdException() {
            super("Id cannot have a blank value.");
        }

    }


}
