package org.rodrigoperalta.devtest.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.rodrigoperalta.devtest.application.config.exceptions.GenericDomainException;

import java.util.Objects;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Product {

    Id id;

    Name name;

    Price price;

    boolean availability;

    public static Product create(
        final Id id, final Name name, final Price price, final boolean availability
    ) {

        validateNullFields(id, name, price);

        return new Product(id, name, price, availability);

    }


    private static void validateNullFields(final Id id, final Name name, final Price price) {

        if (Objects.isNull(id)) {
            throw new NullIdException();
        }

        if (Objects.isNull(name)) {
            throw new NullNameException();
        }

        if (Objects.isNull(price)) {
            throw new NullPriceException();
        }

    }

    public String getIdValue() {
        return this.id.getValue();
    }

    public String getNameValue() {
        return this.name.getValue();
    }

    public Double getPriceValue() {
        return this.price.getValue();
    }

    public static class NullIdException extends GenericDomainException {

        private NullIdException() {
            super("Product cannot have a null id.");
        }

    }

    public static class NullNameException extends GenericDomainException {

        private NullNameException() {
            super("Product cannot have a null name.");
        }

    }

    public static class NullPriceException extends GenericDomainException {

        private NullPriceException() {
            super("Product cannot have a null price.");
        }

    }

}
