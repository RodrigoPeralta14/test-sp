package org.rodrigoperalta.devtest.domain.model;

import lombok.Value;
import org.rodrigoperalta.devtest.application.config.exceptions.GenericDomainException;

import java.util.Objects;

@Value
public class Name {

    String value;

    public static Name create(final String name) {

        if (Objects.isNull(name)) {
            throw new NullNameException();
        }

        if (name.isBlank()) {
            throw new BlankNameException();
        }

        return new Name(name);

    }

    public static class NullNameException extends GenericDomainException {

        private NullNameException() {
            super("Name cannot have a null value.");
        }

    }


    public static class BlankNameException extends GenericDomainException {

        private BlankNameException() {
            super("Name cannot have a blank value.");
        }

    }

}
