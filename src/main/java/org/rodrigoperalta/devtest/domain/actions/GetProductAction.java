package org.rodrigoperalta.devtest.domain.actions;

import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.domain.model.Product;
import org.rodrigoperalta.devtest.infrastructure.feign.SimilarProductsService;
import org.springframework.stereotype.Service;

@Service
public class GetProductAction {

    private final SimilarProductsService similarProductsService;

    public GetProductAction(final SimilarProductsService similarProductsService) {

        this.similarProductsService = similarProductsService;

    }

    public Product execute(final Id id) {

        return similarProductsService.findProductById(id);

    }

}
