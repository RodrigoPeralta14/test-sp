package org.rodrigoperalta.devtest.domain.actions;

import lombok.extern.slf4j.Slf4j;
import org.rodrigoperalta.devtest.application.config.exceptions.NotFoundException;
import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.domain.model.Product;
import org.rodrigoperalta.devtest.infrastructure.feign.SimilarProductsService;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class GetSimilarProductsAction {

    private final GetProductAction getProductAction;
    private final SimilarProductsService similarProductsService;

    public GetSimilarProductsAction(
        final GetProductAction getProductAction,
        final SimilarProductsService similarProductsService
    ) {

        this.getProductAction = getProductAction;
        this.similarProductsService = similarProductsService;

    }

    public Set<Product> execute(final Id productId) {

        final var originalProduct = getProductAction.execute(productId);

        final var similarProducts = similarProductsService.findSimilarsById(
            originalProduct.getId()
        );

        return similarProducts
            .parallelStream()
            .map(String::valueOf)
            .map(id -> {

                try {

                    return getProductAction.execute(
                        Id.create(id)
                    );

                } catch (NotFoundException e) {

                    return null;

                }

            })
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());

    }

}
