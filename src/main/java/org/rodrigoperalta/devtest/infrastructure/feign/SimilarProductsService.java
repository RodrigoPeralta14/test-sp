package org.rodrigoperalta.devtest.infrastructure.feign;

import org.rodrigoperalta.devtest.application.config.exceptions.NotFoundException;
import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.domain.model.Product;
import org.rodrigoperalta.devtest.infrastructure.feign.mapper.ProductDtoToDomainMapper;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class SimilarProductsService {

    private final SimilarProductsApi similarProductsApi;

    public SimilarProductsService(
        final SimilarProductsApi similarProductsApi
    ) {
        this.similarProductsApi = similarProductsApi;
    }

    public Product findProductById(final Id id) {

        final var productDto = similarProductsApi.getProduct(id.getValue());
        return ProductDtoToDomainMapper.mapToDomain(productDto);

    }

    public Set<Number> findSimilarsById(final Id id) {

        try {

            return new HashSet<>(
                similarProductsApi.getProductSimilarIds(id.getValue())
            );

        } catch(NotFoundException e) {

            return Collections.emptySet();

        }

    }

}
