package org.rodrigoperalta.devtest.infrastructure.feign.mapper;

import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.domain.model.Name;
import org.rodrigoperalta.devtest.domain.model.Price;
import org.rodrigoperalta.devtest.domain.model.Product;
import org.rodrigoperalta.devtest.infrastructure.feign.response.ProductDto;

public class ProductDtoToDomainMapper {

    public static Product mapToDomain(final ProductDto productDto) {

        final var id = Id.create(productDto.getId());
        final var name = Name.create(productDto.getName());
        final var price = Price.create(productDto.getPrice());
        final var availability = productDto.isAvailability();

        return Product.create(id, name, price, availability);

    }

}
