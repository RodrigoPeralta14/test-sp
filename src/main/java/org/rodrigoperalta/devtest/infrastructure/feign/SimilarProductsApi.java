package org.rodrigoperalta.devtest.infrastructure.feign;

import org.rodrigoperalta.devtest.infrastructure.feign.response.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(
    value = "SimilarProductsApi",
    url = "${application.feign.similar-products-api}"
)
public interface SimilarProductsApi {

    @GetMapping(value = "/product/{idProduct}", produces = "application/json")
    ProductDto getProduct(
        @PathVariable("idProduct") final String idProduct
    );

    @GetMapping(value = "/product/{idProduct}/similarids", produces = "application/json")
    List<Number> getProductSimilarIds(
        @PathVariable("idProduct") final String idProduct
    );
}
