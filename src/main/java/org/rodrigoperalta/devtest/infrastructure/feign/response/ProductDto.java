package org.rodrigoperalta.devtest.infrastructure.feign.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    @NotNull
    @Size(min = 1)
    @JsonProperty(value = "id")
    private String id;

    @NotNull
    @Size(min = 1)
    @JsonProperty(value = "name")
    private String name;

    @NotNull
    @JsonProperty(value = "price")
    private double price;

    @NotNull
    @JsonProperty(value = "availability")
    private boolean availability;

}
