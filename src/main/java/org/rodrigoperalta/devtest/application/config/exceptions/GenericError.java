package org.rodrigoperalta.devtest.application.config.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class GenericError extends RuntimeException {

    protected GenericError(final String message) {
        super(
            message
        );
    }


    protected GenericError(final String message, final Object... args) {
        super(
            String.format(message, args)
        );
    }

    protected GenericError(final Throwable cause, final String message, final Object... args) {
        super(
            String.format(message, args), cause
        );
    }
}
