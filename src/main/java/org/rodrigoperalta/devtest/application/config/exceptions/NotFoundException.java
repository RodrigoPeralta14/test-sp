package org.rodrigoperalta.devtest.application.config.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends GenericHttpError {

    public NotFoundException() {
        super(404, "No message available");
    }

    public NotFoundException(String message) {
        super(404, message);
    }

    public NotFoundException(String message, Object... args) {
        super(404, message, args);
    }

    public NotFoundException(Throwable cause, String message, Object... args) {
        super(404, cause, message, args);
    }

}
