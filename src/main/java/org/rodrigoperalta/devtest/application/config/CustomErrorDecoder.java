package org.rodrigoperalta.devtest.application.config;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.rodrigoperalta.devtest.application.config.exceptions.NotFoundException;
import org.rodrigoperalta.devtest.application.config.exceptions.ServiceUnavailableException;
import org.springframework.stereotype.Component;

@Component
public class CustomErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {

        switch (response.status()) {
            case 404:
                return new NotFoundException(response.reason());
            default:
                return new ServiceUnavailableException(response.reason());
        }
    }

}
