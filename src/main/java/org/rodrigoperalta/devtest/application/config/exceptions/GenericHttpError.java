package org.rodrigoperalta.devtest.application.config.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Getter
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public abstract class GenericHttpError extends GenericError {

    private final ZonedDateTime timestampUtc;
    private final int responseStatusCode;

    protected GenericHttpError(
        final int responseStatusCode,
        final String message,
        final Object... args
    ) {

        super(message, args);
        this.responseStatusCode = responseStatusCode;
        this.timestampUtc = ZonedDateTime.now(ZoneOffset.UTC);

    }

    protected GenericHttpError(
        final int responseStatusCode,
        final Throwable cause,
        final String message,
        final Object... args) {

        super(cause, message, args);
        this.responseStatusCode = responseStatusCode;
        this.timestampUtc = ZonedDateTime.now(ZoneOffset.UTC);

    }

    protected GenericHttpError(
        final int responseStatusCode,
        final String message
    ) {

        super(message);
        this.responseStatusCode = responseStatusCode;
        this.timestampUtc = ZonedDateTime.now(ZoneOffset.UTC);
    }
}
