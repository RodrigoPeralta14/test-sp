package org.rodrigoperalta.devtest.application.config.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends GenericHttpError {

    public InternalServerException(String message, Object... args) {
        super(500, message, args);
    }

    public InternalServerException(Throwable cause, String message, Object... args) {
        super(500, cause, message, args);
    }

    public InternalServerException(String message) {
        super(500, message);
    }
}
