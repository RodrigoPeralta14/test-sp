package org.rodrigoperalta.devtest.application.config.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class GenericDomainException extends RuntimeException {

    public GenericDomainException(String message, Object... args) {
        super(
            String.format(message, args)
        );
    }

    public GenericDomainException(String message) {
        super(message);
    }

}
