package org.rodrigoperalta.devtest.application.config.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class ServiceUnavailableException extends GenericHttpError {


    public ServiceUnavailableException() {
        super(503, "No message available");
    }


    public ServiceUnavailableException(String message, Object... args) {
        super(503, message, args);
    }

    public ServiceUnavailableException(Throwable cause, String message, Object... args) {
        super(503, cause, message, args);
    }

    public ServiceUnavailableException(String message) {
        super(503, message);
    }
}
