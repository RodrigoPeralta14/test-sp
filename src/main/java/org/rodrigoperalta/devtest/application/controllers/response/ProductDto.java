package org.rodrigoperalta.devtest.application.controllers.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.rodrigoperalta.devtest.domain.model.Product;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class ProductDto {

    @NotNull
    @Size(min = 1)
    @JsonProperty(value = "id")
    private String id;

    @NotNull
    @Size(min = 1)
    @JsonProperty(value = "name")
    private String name;

    @NotNull
    @JsonProperty(value = "price")
    private double price;

    @NotNull
    @JsonProperty(value = "availability")
    private boolean availability;

    public static ProductDto fromDomain(final Product product) {

        return new ProductDto(
            product.getIdValue(),
            product.getNameValue(),
            product.getPriceValue(),
            product.isAvailability()
        );

    }
}
