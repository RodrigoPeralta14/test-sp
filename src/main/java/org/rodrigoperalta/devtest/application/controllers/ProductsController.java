package org.rodrigoperalta.devtest.application.controllers;

import org.rodrigoperalta.devtest.application.controllers.response.ProductDto;
import org.rodrigoperalta.devtest.domain.actions.GetSimilarProductsAction;
import org.rodrigoperalta.devtest.domain.model.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("product")
public class ProductsController {

    private final GetSimilarProductsAction getSimilarProductsAction;

    @Autowired
    public ProductsController(
        final GetSimilarProductsAction getSimilarProductsAction
    ) {

        this.getSimilarProductsAction = getSimilarProductsAction;

    }

    @GetMapping("{productId}/similar")
    public ResponseEntity<Set<ProductDto>> getSimilarProducts(
        @PathVariable String productId
    ) {

        return ResponseEntity.ok(
            this.getSimilarProductsAction.execute(
                    Id.create(productId)
                )
                .stream()
                .map(ProductDto::fromDomain)
                .collect(Collectors.toSet())
        );

    }

}
