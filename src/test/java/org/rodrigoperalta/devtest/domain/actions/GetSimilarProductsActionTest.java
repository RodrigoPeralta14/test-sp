package org.rodrigoperalta.devtest.domain.actions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.rodrigoperalta.devtest.application.config.exceptions.NotFoundException;
import org.rodrigoperalta.devtest.application.config.exceptions.ServiceUnavailableException;
import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.infrastructure.feign.SimilarProductsService;
import org.rodrigoperalta.devtest.utils.MockUtils;

import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class GetSimilarProductsActionTest {

    private final GetProductAction getProductAction;
    private final GetSimilarProductsAction getSimilarProductsAction;
    private final SimilarProductsService similarProductsService;

    public GetSimilarProductsActionTest() {

        this.getProductAction = mock(GetProductAction.class);
        this.similarProductsService = mock(SimilarProductsService.class);
        this.getSimilarProductsAction = new GetSimilarProductsAction(
            getProductAction, similarProductsService
        );
    }

    @Nested
    @DisplayName("Given a valid existing product id with two valid similar products ids")
    class GivenAValidExistingProductId {

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @DisplayName("Then should return similar products")
            void thenShouldReturnSimilarProducts() {

                // given
                final var productToFind = MockUtils.givenValidProductWithId("1");
                final var productToFind2 = MockUtils.givenValidProductWithId("2");
                final var productToFind3 = MockUtils.givenValidProductWithId("3");

                given(getProductAction.execute(productToFind.getId()))
                    .willReturn(productToFind);

                given(similarProductsService.findSimilarsById(productToFind.getId()))
                    .willReturn(Set.of(2, 3));

                given(getProductAction.execute(productToFind2.getId()))
                    .willReturn(productToFind2);

                given(getProductAction.execute(productToFind3.getId()))
                    .willReturn(productToFind3);

                // when
                final var similarProducts = getSimilarProductsAction.execute(
                    productToFind.getId()
                );

                // then
                assertThat(similarProducts)
                    .isNotNull()
                    .isNotEmpty()
                    .hasSize(2);

                verify(getProductAction, times(3))
                    .execute(any(Id.class));

                verify(similarProductsService, times(1))
                    .findSimilarsById(any(Id.class));

            }

        }

    }

    @Nested
    @DisplayName("Given an invalid non existing product id")
    class GivenAnInvalidNonExistingProductId {

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                final var productToFind = MockUtils.givenValidProductWithId("1");

                given(getProductAction.execute(productToFind.getId()))
                    .willThrow(new NotFoundException());

                // when
                final var throwable =
                    catchThrowableOfType(
                        () -> getSimilarProductsAction.execute(
                            productToFind.getId()
                        ),
                        NotFoundException.class
                    );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("No message available");

                verify(getProductAction, times(1))
                    .execute(any(Id.class));

                verify(similarProductsService, times(0))
                    .findSimilarsById(any(Id.class));

            }

        }

    }

    @Nested
    @DisplayName("Given a valid existing product id with two valid similar products ids but one is not found")
    class GivenAValidExistingProductIdButOneSimilarProductIsNotFound {

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @DisplayName("Then should return only one similar products")
            void thenShouldReturnOnlyOneSimilarProducts() {

                // given
                final var productToFind = MockUtils.givenValidProductWithId("1");
                final var productToFind2 = MockUtils.givenValidProductWithId("2");
                final var productToFind3 = MockUtils.givenValidProductWithId("3");

                given(getProductAction.execute(productToFind.getId()))
                    .willReturn(productToFind);

                given(similarProductsService.findSimilarsById(productToFind.getId()))
                    .willReturn(Set.of(2, 3));

                given(getProductAction.execute(productToFind2.getId()))
                    .willReturn(productToFind2);

                given(getProductAction.execute(productToFind3.getId()))
                    .willThrow(new NotFoundException());

                // when
                final var similarProducts = getSimilarProductsAction.execute(
                    productToFind.getId()
                );

                // then
                assertThat(similarProducts)
                    .isNotNull()
                    .isNotEmpty()
                    .hasSize(1);

                verify(getProductAction, times(3))
                    .execute(any(Id.class));

                verify(similarProductsService, times(1))
                    .findSimilarsById(any(Id.class));

            }

        }

    }

    @Nested
    @DisplayName("Given a valid existing product id with two valid similar products ids but one is service unavailable")
    class GivenAValidExistingProductIdButOneSimilarProductIsServiceNotAvailable {

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                final var productToFind = MockUtils.givenValidProductWithId("1");
                final var productToFind2 = MockUtils.givenValidProductWithId("2");
                final var productToFind3 = MockUtils.givenValidProductWithId("3");

                given(getProductAction.execute(productToFind.getId()))
                    .willReturn(productToFind);

                given(similarProductsService.findSimilarsById(productToFind.getId()))
                    .willReturn(Set.of(2, 3));

                given(getProductAction.execute(productToFind2.getId()))
                    .willReturn(productToFind2);

                given(getProductAction.execute(productToFind3.getId()))
                    .willThrow(new ServiceUnavailableException());

                // when
                final var throwable =
                    catchThrowableOfType(
                        () -> getSimilarProductsAction.execute(
                            productToFind.getId()
                        ),
                        ServiceUnavailableException.class
                    );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("No message available");

                verify(getProductAction, times(3))
                    .execute(any(Id.class));

                verify(similarProductsService, times(1))
                    .findSimilarsById(any(Id.class));

            }

        }

    }

    @Nested
    @DisplayName("Given a valid existing product id with two valid similar products ids but none is found")
    class GivenAValidExistingProductIdButNoSimilarProductsAreFound {

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @DisplayName("Then should return only no similar products")
            void thenShouldReturnNoSimilarProducts() {

                // given
                final var productToFind = MockUtils.givenValidProductWithId("1");
                final var productToFind2 = MockUtils.givenValidProductWithId("2");
                final var productToFind3 = MockUtils.givenValidProductWithId("3");

                given(getProductAction.execute(productToFind.getId()))
                    .willReturn(productToFind);

                given(similarProductsService.findSimilarsById(productToFind.getId()))
                    .willReturn(Set.of(2, 3));

                given(getProductAction.execute(productToFind2.getId()))
                    .willThrow(new NotFoundException());

                given(getProductAction.execute(productToFind3.getId()))
                    .willThrow(new NotFoundException());

                // when
                final var similarProducts = getSimilarProductsAction.execute(
                    productToFind.getId()
                );

                // then
                assertThat(similarProducts)
                    .isNotNull()
                    .isEmpty();

                verify(getProductAction, times(3))
                    .execute(any(Id.class));

                verify(similarProductsService, times(1))
                    .findSimilarsById(any(Id.class));

            }

        }

    }

    @Nested
    @DisplayName("Given a valid existing product id with no valid similar ids")
    class GivenAValidExistingProductIdWithNoValidSimilarIds {

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @DisplayName("Then should return no similar products")
            void thenShouldReturnNoSimilarProducts() {

                // given
                final var productToFind = MockUtils.givenValidProductWithId("1");

                given(getProductAction.execute(productToFind.getId()))
                    .willReturn(productToFind);

                given(similarProductsService.findSimilarsById(productToFind.getId()))
                    .willReturn(Collections.emptySet());

                // when
                final var similarProducts = getSimilarProductsAction.execute(
                    productToFind.getId()
                );

                // then
                assertThat(similarProducts)
                    .isNotNull()
                    .isEmpty();

                verify(getProductAction, times(1))
                    .execute(any(Id.class));

                verify(similarProductsService, times(1))
                    .findSimilarsById(any(Id.class));

            }

        }

    }

}
