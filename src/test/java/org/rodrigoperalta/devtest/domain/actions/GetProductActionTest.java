package org.rodrigoperalta.devtest.domain.actions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.rodrigoperalta.devtest.application.config.exceptions.NotFoundException;
import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.infrastructure.feign.SimilarProductsService;
import org.rodrigoperalta.devtest.utils.MockUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class GetProductActionTest {

    private final GetProductAction getProductAction;
    private final SimilarProductsService similarProductsService;

    public GetProductActionTest() {

        this.similarProductsService = mock(SimilarProductsService.class);
        this.getProductAction = new GetProductAction(
            similarProductsService
        );

    }

    @Nested
    @DisplayName("Given a valid product id")
    class GivenAValidProductId {

        @Nested
        @DisplayName("When obtaining product")
        class WhenObtainingProduct {

            @Test
            @DisplayName("Then should return Product")
            void thenShouldReturnProduct() {

                // given
                final var id = Id.create("1");
                final var returnedProduct = MockUtils.givenValidProductWithName("a-product");

                given(similarProductsService.findProductById(id))
                    .willReturn(returnedProduct);

                // when
                final var product = getProductAction.execute(id);

                // then
                assertThat(product)
                    .isNotNull();


            }

        }

    }

    @Nested
    @DisplayName("Given a not found product id")
    class GivenANotFoundProductId {

        @Nested
        @DisplayName("When obtaining product")
        class WhenObtainingProduct {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                final var id = Id.create("1");
                final var returnedProduct = MockUtils.givenValidProductWithName("a-product");

                given(similarProductsService.findProductById(id))
                    .willThrow(
                        new NotFoundException("Product with id %s not found", id.getValue())
                    );

                // when
                final var throwable = catchThrowableOfType(
                    () -> getProductAction.execute(id),
                    NotFoundException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull();

            }

        }

    }

}
