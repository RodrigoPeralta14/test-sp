package org.rodrigoperalta.devtest.domain.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.rodrigoperalta.devtest.domain.model.Product.*;

class ProductTest {

    @Nested
    @DisplayName("Given a valid id, a valid name, a valid price and a valid availability")
    class GivenAValidIdAndAValidNameAndAValidPriceAndAValidAvailability {

        private final Id id = Id.create("an-id");
        private final Name name = Name.create("a-name");
        private final Price price = Price.create(1000.0);
        private final boolean availability = true;

        @Nested
        @DisplayName("When creating new product")
        class WhenCreatingNewProduct {

            @Test
            @DisplayName("Then should return valid created product")
            void thenShouldReturnValidCreatedProduct() {

                // when
                final var product = create(
                    id, name, price, availability
                );

                // then
                assertThat(product)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

            }

        }

    }

    @Nested
    @DisplayName("Given a null id, a valid name, a valid price and a valid availability")
    class GivenANullIdAndAValidNameAndAValidPriceAndAValidAvailability {

        private final Id id = null;
        private final Name name = Name.create("a-name");
        private final Price price = Price.create(1000.0);
        private final boolean availability = true;

        @Nested
        @DisplayName("When creating new product")
        class WhenCreatingNewProduct {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> create(
                        id, name, price, availability
                    ),
                    NullIdException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Product cannot have a null id.");

            }

        }

    }

    @Nested
    @DisplayName("Given a valid id, a null name, a valid price and a valid availability")
    class GivenAValidIdAndANullNameAndAValidPriceAndAValidAvailability {

        private final Id id = Id.create("an-id");
        private final Name name = null;
        private final Price price = Price.create(1000.0);
        private final boolean availability = true;

        @Nested
        @DisplayName("When creating new product")
        class WhenCreatingNewProduct {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> create(
                        id, name, price, availability
                    ),
                    NullNameException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Product cannot have a null name.");

            }

        }

    }

    @Nested
    @DisplayName("Given a valid id, a valid name, a null price and a valid availability")
    class GivenAValidIdAndAValidNameAndANullPriceAndAValidAvailability {

        private final Id id = Id.create("an-id");
        private final Name name = Name.create("a-name");
        private final Price price = null;
        private final boolean availability = true;

        @Nested
        @DisplayName("When creating new product")
        class WhenCreatingNewProduct {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> create(
                        id, name, price, availability
                    ),
                    NullPriceException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Product cannot have a null price.");

            }

        }

    }

}
