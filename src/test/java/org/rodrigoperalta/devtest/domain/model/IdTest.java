package org.rodrigoperalta.devtest.domain.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;

class IdTest {

    @Nested
    @DisplayName("Given a valid string value")
    class GivenAValidStringValue {

        @Nested
        @DisplayName("When creating id value object")
        class WhenCreatingIdValueObject {

            @Test
            @DisplayName("Then should return Id value Object")
            void thenShouldReturnIdValueObject() {

                // given
                final var value = "sarasa";

                // when
                final var id = Id.create(value);

                // then
                assertThat(id)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

                assertThat(id.getValue())
                    .isEqualTo(value);


            }

        }

    }

    @Nested
    @DisplayName("Given a null string value")
    class GivenANullStringValue {

        @Nested
        @DisplayName("When creating id value object")
        class WhenCreatingIdValueObject {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> Id.create(null),
                    Id.NullIdException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Id cannot have a null value.");


            }

        }

    }

    @Nested
    @DisplayName("Given a blank string value")
    class GivenABlankStringValue {

        @Nested
        @DisplayName("When creating id value object")
        class WhenCreatingIdValueObject {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                final var throwable = catchThrowableOfType(
                    () -> Id.create("              "),
                    Id.BlankIdException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Id cannot have a blank value.");

            }

        }

    }

}
