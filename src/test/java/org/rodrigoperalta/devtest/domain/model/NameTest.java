package org.rodrigoperalta.devtest.domain.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;

class NameTest {

    @Nested
    @DisplayName("Given a valid string value")
    class GivenAValidStringValue {

        @Nested
        @DisplayName("When creating name value object")
        class WhenCreatingNameValueObject {

            @Test
            @DisplayName("Then should return name value Object")
            void thenShouldReturnNameValueObject() {

                // given
                final var value = "sarasa";

                // when
                final var name = Name.create(value);

                // then
                assertThat(name)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

                assertThat(name.getValue())
                    .isEqualTo(value);


            }

        }

    }

    @Nested
    @DisplayName("Given a null string value")
    class GivenANullStringValue {

        @Nested
        @DisplayName("When creating name value object")
        class WhenCreatingNameValueObject {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> Name.create(null),
                    Name.NullNameException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Name cannot have a null value.");


            }

        }

    }

    @Nested
    @DisplayName("Given a blank string value")
    class GivenABlankStringValue {

        @Nested
        @DisplayName("When creating name value object")
        class WhenCreatingNameValueObject {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> Name.create("             "),
                    Name.BlankNameException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Name cannot have a blank value.");


            }

        }

    }

}
