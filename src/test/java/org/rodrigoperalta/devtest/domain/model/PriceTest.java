package org.rodrigoperalta.devtest.domain.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;

class PriceTest {

    @Nested
    @DisplayName("Given a valid number value")
    class GivenAValidNumberValue {

        @Nested
        @DisplayName("When creating price value object")
        class WhenCreatingPriceValueObject {

            @Test
            @DisplayName("Then should return price value Object")
            void thenShouldReturnPriceValueObject() {

                // given
                final var value = 654654.0;

                // when
                final var price = Price.create(value);

                // then
                assertThat(price)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

                assertThat(price.getValue())
                    .isEqualTo(value);

            }

        }

    }

    @Nested
    @DisplayName("Given a null number value")
    class GivenANullNumberValue {

        @Nested
        @DisplayName("When creating price value object")
        class WhenCreatingPriceValueObject {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> Price.create(null),
                    Price.NullPriceException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Price cannot have a null value.");


            }

        }

    }

    @Nested
    @DisplayName("Given a negative number value")
    class GivenANegativeNumberValue {

        @Nested
        @DisplayName("When creating price value object")
        class WhenCreatingPriceValueObject {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // when
                final var throwable = catchThrowableOfType(
                    () -> Price.create(-1.0),
                    Price.NegativePriceException.class
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Price cannot have a negative value.");


            }

        }

    }

}
