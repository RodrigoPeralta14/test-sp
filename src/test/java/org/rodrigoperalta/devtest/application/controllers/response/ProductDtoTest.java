package org.rodrigoperalta.devtest.application.controllers.response;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.rodrigoperalta.devtest.utils.MockUtils;

import static org.assertj.core.api.Assertions.assertThat;

class ProductDtoTest {

    @Nested
    @DisplayName("Given a valid id, a valid name, a valid price and a valid availability")
    class GivenAValidIdAndAValidNameAndAValidNumberAndAValidAvailability {

        private final String id = "an-id";
        private final String name = "a-name";
        private final Long price = 1000L;
        private final boolean availability = true;

        @Nested
        @DisplayName("When creating product dto")
        class WhenCreatingProductDto {

            @Test
            @DisplayName("Then should create product dto")
            void thenShouldCreateProductDto() {

                // when
                final var productDto = new ProductDto(
                    id, name, price, availability
                );

                // then
                assertThat(productDto)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

            }

        }

    }

    @Nested
    @DisplayName("Given a valid product")
    class GivenAValidProduct {

        @Nested
        @DisplayName("When converting from domain")
        class WhenConvertingFromDomain {

            @Test
            @DisplayName("Then should return product dto")
            void thenShouldReturnProductDto() {

                // given
                final var product = MockUtils.givenValidProductWithName(
                    "some-name"
                );

                // when
                final var productDto = ProductDto.fromDomain(product);

                // then
                assertThat(productDto)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

                assertThat(productDto.getId())
                    .isEqualTo(product.getIdValue());

                assertThat(productDto.getPrice())
                    .isEqualTo(product.getPriceValue());

                assertThat(productDto.getName())
                    .isEqualTo(product.getNameValue());

                assertThat(productDto.isAvailability())
                    .isEqualTo(product.isAvailability());

            }

        }

    }

}
