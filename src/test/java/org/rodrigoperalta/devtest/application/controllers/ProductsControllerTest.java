package org.rodrigoperalta.devtest.application.controllers;

import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.rodrigoperalta.devtest.application.config.exceptions.NotFoundException;
import org.rodrigoperalta.devtest.application.config.exceptions.ServiceUnavailableException;
import org.rodrigoperalta.devtest.domain.actions.GetSimilarProductsAction;
import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.utils.MockUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@MockBean({GetSimilarProductsAction.class})
@WebMvcTest(controllers = ProductsController.class)
@DisplayName("Controller unit test w/ spring suite")
class ProductsControllerTest {

    private final GetSimilarProductsAction getSimilarProductsAction;
    private final ProductsController productsController;
    private MockMvc mockMvc;

    @Autowired
    public ProductsControllerTest(
        final MockMvc mockMvc,
        final ProductsController productsController,
        final GetSimilarProductsAction getSimilarProductsAction
    ) {
        this.mockMvc = mockMvc;
        this.productsController = productsController;
        this.getSimilarProductsAction = getSimilarProductsAction;
    }

    @Nested
    @DisplayName("Given a product id of a product that exists")
    class GivenAProductThatExists {

        private final String idProduct = "1";

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @SneakyThrows
            @DisplayName("Then a list of similar products should be returned")
            void thenAListOfSimilarProductsShouldBeReturned() {

                // given
                given(
                    getSimilarProductsAction.execute(any(Id.class))
                )
                    .willReturn(
                        MockUtils.givenSetOfProducts()
                    );

                // when
                var result = mockMvc.perform(
                        get("/product/" + idProduct + "/similar")
                            .contentType(MediaType.APPLICATION_JSON_VALUE)

                    )
                    .andDo(print());

                final var response = result.andReturn().getResponse();

                // then
                assertThat(response)
                    .as("HTTP response")
                    .isNotNull();

                assertThat(response.getStatus())
                    .isEqualTo(200);

                // TODO map response and check that list has 2 products

                verify(getSimilarProductsAction, times(1))
                    .execute(any(Id.class));

            }

        }

    }

    @Nested
    @DisplayName("Given a product id of a product that does not exist")
    class GivenAProductThatDoesNotExist {

        private final String idProduct = "1";

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @SneakyThrows
            @DisplayName("Then should return 404")
            void thenShouldReturn404() {

                // given
                given(
                    getSimilarProductsAction.execute(any(Id.class))
                )
                    .willThrow(
                        new NotFoundException()
                    );

                // when
                var result = mockMvc.perform(
                        get("/product/" + idProduct + "/similar")
                            .contentType(MediaType.APPLICATION_JSON_VALUE)

                    )
                    .andDo(print());

                // then
                final var response = result.andReturn().getResponse();

                assertThat(response)
                    .as("HTTP response")
                    .isNotNull();

                assertThat(response.getStatus())
                    .isEqualTo(404);

                verify(getSimilarProductsAction, times(1))
                    .execute(any(Id.class));

            }

        }

    }

    @Nested
    @DisplayName("Given a valid product but transaction fails")
    class GivenAValidProductButTransactionFails {

        private final String idProduct = "1";

        @Nested
        @DisplayName("When obtaining similar products")
        class WhenObtainingSimilarProducts {

            @Test
            @SneakyThrows
            @DisplayName("Then should return 503")
            void thenShouldReturn503() {

                // given
                given(
                    getSimilarProductsAction.execute(any(Id.class))
                )
                    .willThrow(
                        new ServiceUnavailableException()
                    );

                // when
                var result = mockMvc.perform(
                        get("/product/" + idProduct + "/similar")
                            .contentType(MediaType.APPLICATION_JSON_VALUE)

                    )
                    .andDo(print());

                // then
                final var response = result.andReturn().getResponse();

                assertThat(response)
                    .as("HTTP response")
                    .isNotNull();

                assertThat(response.getStatus())
                    .isEqualTo(503);

                verify(getSimilarProductsAction, times(1))
                    .execute(any(Id.class));

            }

        }

    }

}
