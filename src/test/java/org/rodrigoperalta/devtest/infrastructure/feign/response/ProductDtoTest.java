package org.rodrigoperalta.devtest.infrastructure.feign.response;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.rodrigoperalta.devtest.application.controllers.response.ProductDto;

import static org.assertj.core.api.Assertions.assertThat;

class ProductDtoTest {

    @Nested
    @DisplayName("Given a valid id, a valid name, a valid price and a valid availability")
    class GivenAValidIdAndAValidNameAndAValidNumberAndAValidAvailability {

        private final String id = "an-id";
        private final String name = "a-name";
        private final double price = 3000.0;
        private final boolean availability = true;

        @Nested
        @DisplayName("When creating product dto")
        class WhenCreatingProductDto {

            @Test
            @DisplayName("Then should create product dto")
            void thenShouldCreateProductDto() {

                // when
                final var productDto = new ProductDto(
                    id, name, price, availability
                );

                // then
                assertThat(productDto)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

            }

        }

    }

}
