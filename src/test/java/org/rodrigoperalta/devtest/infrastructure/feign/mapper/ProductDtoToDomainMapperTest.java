package org.rodrigoperalta.devtest.infrastructure.feign.mapper;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.rodrigoperalta.devtest.utils.MockUtils;

import static org.assertj.core.api.Assertions.assertThat;

class ProductDtoToDomainMapperTest {

    @Nested
    @DisplayName("Given a valid product dto")
    class GivenAValidProductDto {

        @Nested
        @DisplayName("When mapping to domain")
        class WhenMappingToDomain {

            @Test
            @DisplayName("Then should return domain Product value object")
            void thenShouldReturnDomainProductValueObject() {

                // given
                final var productDto = MockUtils.givenValidFeignProductDto();

                // when
                final var product = ProductDtoToDomainMapper.mapToDomain(productDto);

                // then
                assertThat(product)
                    .isNotNull()
                    .hasNoNullFieldsOrProperties();

            }

        }

    }

}
