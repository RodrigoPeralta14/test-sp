package org.rodrigoperalta.devtest.utils;

import org.rodrigoperalta.devtest.domain.model.Id;
import org.rodrigoperalta.devtest.domain.model.Name;
import org.rodrigoperalta.devtest.domain.model.Price;
import org.rodrigoperalta.devtest.domain.model.Product;
import org.rodrigoperalta.devtest.infrastructure.feign.response.ProductDto;

import java.util.Set;
import java.util.UUID;

public class MockUtils {

    public static Set<Product> givenSetOfProducts() {
        return Set.of(
            givenValidProductWithName("a-product"),
            givenValidProductWithName("a-product-2")
        );
    }

    public static Product givenValidProductWithName(String productName) {

        return Product.create(
            givenValidId(UUID.randomUUID().toString()),
            givenValidName(productName),
            givenValidPrice(1000.0),
            true
        );

    }

    public static Product givenValidProductWithId(String id) {

        return Product.create(
            givenValidId(id),
            givenValidName("a-name"),
            givenValidPrice(1000.0),
            true
        );

    }

    private static Id givenValidId(final String id) {
        return Id.create(id);
    }

    private static Name givenValidName(final String name) {
        return Name.create(name);
    }

    private static Price givenValidPrice(final Double price) {
        return Price.create(price);
    }

    public static ProductDto givenValidFeignProductDto() {

        return new ProductDto("an-id", "a-name", 10.5, true);

    }
}
